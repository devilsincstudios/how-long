// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	Used in the main menu, this simply stores a string (the name of the level to load).
// *
// */

using UnityEngine;

namespace DI_Game
{
	[AddComponentMenu("Menus/Main/LevelLoadingButton")]
	public class LevelLoadingButton : MonoBehaviour {
		public string levelName;
	}
}