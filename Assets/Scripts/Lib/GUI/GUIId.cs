// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_GUI
{
	public enum GUIId
	{
		DEBUG_CONSOLE,
		PLAYER_INVENTORY,
		PLAYER_STATUS,
		PLAYER_HOTBAR,
		PLAYER_EQUIPPED,
		CRAFTING_VIEW,
		LOOT_CONTAINER,
		LOOT_ENEMY,
		MODAL_CONFIRM,
	}
}