/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

using System;
using UnityEngine;

namespace DI_GUI
{
	public static class GUILib
	{
		public static GUISkin skin;

		public static GUISkin getSkin()
		{
			if (skin == null) {
				skin = new GUISkin();
				return skin;
			}
			else {
				return skin;
			}
		}
	}
}