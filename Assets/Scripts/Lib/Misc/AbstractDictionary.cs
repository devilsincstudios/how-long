// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;
using System;
using System.Collections.Generic;

namespace DI_Common {
	[System.Serializable]
	public abstract class AbstractDictionary <K,V> : ScriptableObject
	{
		[SerializeField]
		public List<K> keys;
		[SerializeField]
		public List<V> values;

		public Dictionary<K, V> dictionary;

		public AbstractDictionary() {
			dictionary = new Dictionary<K, V>();
			if (keys == null) {
				keys = new List<K>();
				values = new List<V>();
			}
		}

		public void OnEnable() {
			dictionary = new Dictionary<K, V>();
			for(int index = 0; index < keys.Count; index++) {
				dictionary.Add(keys[index], values[index]);
			}
			keys.Clear();
			values.Clear();
		}

		public void Serialize() {
			keys.Clear();
			values.Clear();
			foreach(KeyValuePair<K,V> element in dictionary) {
				keys.Add(element.Key);
				values.Add(element.Value);
			}
		}
	}
}
