/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

using UnityEngine;
using System.Collections;
using System;

/*
 * Add to this file as more are introduced.
 * It can all be implemented later.
 * Codes:
 * 
 * Misc:
 * I Have Kids - Unlimited Saves.
 * Im Invincible - More Hordes.
 * Im Getting to Old For This - PDA pauses the game when open.
 * Spokes Hare - Unlimited battery life on the pda.
 * We're in the pipe 5 by 5 - Unlimited Health.
 * Tips Welcome - Unlimited Money.
 * I Like a Do Da Cha Cha - Enemies will ignore you.
 * Like a Boss - One hit kill any enemy.
 * Tool Time - Unlimited Durability.
 * Okay I'm Reloaded - Unlimited Ammo.
 * 
 * Stats:
 * They'll never see me coming - Max Sneak
 * Hercules Hercules - Max Strength
 * Wind Walker - Max Agility
 * Ten Steps Ahead - Max Intelligence
 * They Said OCD Was a Bad Thing - Max Sense
 * 
 * Bad Effects:
 * I'm Hardcore - Reset stats to 0.
 * */

namespace DI_Input {
	public class cheatCodes {
		public void cheatUnlimitedSaves() {
		}
		public void cheatMoreHordes() {
		}
		public void cheatBatteryLife() {
		}
		public void cheatHealth() {
		}
		public void cheatMoney() {
		}
		public void cheatInvisible() {
		}
		public void cheatOneShot() {
		}
		public void cheatDurability() {
		}
		public void cheatAmmo() {
		}
		public void cheatSneak() {
		}
		public void cheatStrength() {
		}
		public void cheatAgility() {
		}
		public void cheatIntelligence() {
		}
		public void cheatSense() {
		}
		public void cheatResetStats() {
		}
		public void disableAchivements() {
		}
	}
}
