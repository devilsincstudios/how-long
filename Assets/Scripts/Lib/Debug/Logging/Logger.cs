/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Debug logger, outputs in json format.
*	Logs are viewed by logviewer.
*	This is a singleton class.
*
*/

using System;
using System.Collections;
using System.Drawing;
using UnityEngine;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

// Logger is a singleton though not static.
namespace DI_Debug {
	public class Logger {
		private static Logger logger;
		private static StreamWriter logWriter;
		public string logFile;
		private int logEntries;

		private Logger() {
			setLogFile();
			logEntries = 0;
		}

		// Open the log file for writing.
		// Called once at the start the game.
		private void openLogFile() {
			try {
				logWriter = File.AppendText(logFile);
				UnityEngine.Debug.Log("Opened log file: " + logFile);
			}
			catch (Exception e) {
				UnityEngine.Debug.Log(e.ToString());
			}
		}

		// Close the log file.
		// Called once at the end of the game.
		private void closeLogFile() {
			// Clean up of log file.
			// Make sure we append the final } so the json is usable.
			if(logEntries > 0) {
				logWriter.Flush();
				logWriter.Close();
				makeUsableJSON();
			}
		}

		// Due to the way JSON is handled we need to remove the last ,
		private void makeUsableJSON() {
			string[] lines = File.ReadAllLines(getLogFile());
			lines[lines.Count() - 1] = "]}";
			File.WriteAllLines(getLogFile(), lines);
			UnityEngine.Debug.Log(lines.ToString());
		}

		// This is the singleton creator.
		// TODO should this be static if the rest isn't?
		public static Logger Instance {
			get {
				if (logger == null) {
					logger = new Logger();
				}
				return logger;
			}
		}

		// Destructor, clean up the log file and close it.
		~Logger() {
			closeLogFile();
		}

		// Returns the logger instance.
		public Logger getLogger() {

			return logger;
		}

		// Takes a screenshot
		public string takeScreenShot() {
			using (ScreenShot ss = new ScreenShot()) {
				return ss.takeScreenShot();
			}
		}

		// Closes the log file.
		public void cleanUp() {
			closeLogFile();
		}

		// Sets the name of the log file.
		// Logs save to Logs\<month>-<day>-<year>
		// If the directory tree doesn't exist it attempts to create it.
		// The log file itself is named Logs\<month>-<day>-<year>\<hour>-<minute>-<second>.json
		private void setLogFile() {
			DateTime date = DateTime.Now;
			string logPath = "Logs\\" + date.Month + "-" + date.Day + "-" + date.Year;
			if (!Directory.Exists(logPath)) {
				Directory.CreateDirectory(logPath);
			}
			logFile = logPath + "\\" + date.Hour + "-" + date.Minute + "-" + date.Second + ".json";
		}

		// Returns the name of the log file.
		public string getLogFile() {
			return logFile;
		}

		// Writes a message to the log file.
		public void log(LogMessage logMessage) {
			// If this is the first entry, we need to add the json opener }
			if (logEntries == 0) {
				openLogFile();
				writeToLogFile("{");
				logWriter.AutoFlush = true;
			}
			// Write the log entry
			// messages using the logMessage object are converted into json format by the logMessage class.
			writeToLogFile(logMessage.toJSON());
			// Increase the entries, this currently only tracks if we need to write the json header.
			++logEntries;
		}

		// Writes a raw string to the log file.
		// This should never be called by anything other than logger as it does not convert to json for you.
		private void writeToLogFile(string message) {
			try {
				logWriter.WriteLine(message);
			}
			catch (Exception error) {
				UnityEngine.Debug.LogException(error);
			}
		}

		// I got tried of creating logMessage objects for each entry.
		// This lets you do your log entry with a single line.
		// It sets the most basic settings for you, if you need more control over the message use the log function.
		public void simpleLog(string priority, string component, string message) {
			// Create our message object.
			LogMessage logMessage = new LogMessage();
			logMessage.setTitle(message);
			logMessage.setMessage(message);
			// Left out for execution time reasons, if you want to add it in to a less frequent log entry then don't use the simpleLog.
			//logMessage.setStackTrace(new StackTrace(true));
			logMessage.setComponent(component);
			logMessage.setLogLevel(priority);
			log (logMessage);

			// We spam the debug console enough, removing this for now.
			//UnityEngine.Debug.Log(message);
		}

		// This should never be called by anything except the console.
		// It generates a test log file.
		public void testLogger() {
			// Add each of the priorites.
			List<string> priorities = new List<string>();
			priorities.Add("Critical");
			priorities.Add("High");
			priorities.Add("Medium");
			priorities.Add("Low");
			priorities.Add("Info");
			// Now add each of the categories.
			List<string> categories = new List<string>();
			categories.Add("AI");
			categories.Add("Client");
			categories.Add("Debug");
			categories.Add("Error");
			categories.Add("Framework");
			categories.Add("Game Logic");
			categories.Add("Graphics");
			categories.Add("Gui");
			categories.Add("Input");
			categories.Add("Network");
			categories.Add("Physics");
			categories.Add("Server");
			categories.Add("System");
			categories.Add("Warning");

			// Create an entry of each priority and type for testing.
			foreach (string priority in priorities.ToArray()) {
				foreach (string category in categories.ToArray()) {
					LogMessage logMessage = new LogMessage();
					logMessage.setTitle("Testing: " + priority + " : " + category);
					logMessage.setMessage("Testing: " + priority + " : " + category + " : Message Text");
					logMessage.setStackTrace(new StackTrace(true));
					logMessage.setLogLevel(priority);
					logMessage.setComponent(category);
					logMessage.setScreenShot(takeScreenShot());
					log (logMessage);
				}
			}
		}
	}
}