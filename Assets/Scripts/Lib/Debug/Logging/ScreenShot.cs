/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

using System;
using System.Collections;
using System.Drawing;
using UnityEngine;
using System.Drawing.Imaging;
using System.IO;

namespace DI_Debug {
	public class ScreenShot : IDisposable {
		// If the user takes the screenshot then we want to let them have easy access to them.
		// If we take it for debug reasons, we wan't to be able to clean up after ourselves without deleting the users screenshots.
		public string takeScreenShot() {
			return processScreenShot(false);
		}
		public string takeScreenShot(bool userInitiated) {
			return processScreenShot(true);
		}

		// Takes a screen shot of the current play area.
		// TODO Rethink this naming convention.
		// Screenshots are saved as Logs\ScreenShots\User\<month>_<day>_<year>_<hour>_<minute>_<millisecond>.png

		private string processScreenShot(bool userInitiated) {
			DateTime date = DateTime.Now;
			string filePath;
			string fileName;
			if (userInitiated == true) {
				filePath = "Logs\\ScreenShots\\User";
				fileName = date.Month + "_" + date.Day + "_" + date.Year + "_" + date.Hour + "_" + date.Minute + "_" + date.Millisecond + ".png";
			}
			else {
				filePath = "Logs\\ScreenShots\\" + date.Month + "_" + date.Day + "_" + date.Year;
				fileName = date.Hour + "_" + date.Minute + "_" + date.Millisecond + ".png";
			}

			if (!Directory.Exists(filePath)) {
				Directory.CreateDirectory(filePath);
			}
			using (Bitmap bitmap = new Bitmap(Screen.width, Screen.height)) {
				using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap)) {
					Size bounds = new Size(Screen.width, Screen.height);
					g.CopyFromScreen(Point.Empty, Point.Empty, bounds);
				}
				
				bitmap.Save(filePath + "\\" + fileName, ImageFormat.Png);
			}
			return (filePath + "\\" + fileName);
		}

		public void Dispose() {
			GC.SuppressFinalize(this);
		}

		~ScreenShot() {
			Dispose();
		}
	}
}