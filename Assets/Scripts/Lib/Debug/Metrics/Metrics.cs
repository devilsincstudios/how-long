/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/


/*
 * 
 * FIXME This is currently not reporting the correct values for cpu / ram usage.
 * FIXME Will require research into mono specifc perfcounter behaviour as this should work for .NET
 * TODO Expand this to allow for sending to debug console as well as reporting to graphs.
 *
*/

using System;
using System.Diagnostics;
using UnityEngine;
using System.Collections;

// NOTE This will need to be reworked after we have unity pro.
// For now, just leave it as is.

namespace DI_Debug {
	public class Metrics {
		System.Diagnostics.PerformanceCounter perfCounter;
		GameObject debugConsole;

		public Metrics() {
			debugConsole = GameObject.FindGameObjectWithTag("Console");
		}

		public IEnumerator getCurrentCpuUsage(){
			perfCounter = new System.Diagnostics.PerformanceCounter();
			perfCounter.CategoryName = "Processor";
			perfCounter.CounterName = "% Processor Time";
			perfCounter.InstanceName = "_Total";
			perfCounter.NextValue();
			for (int iteration = 0; iteration < 5; ++iteration) {
				yield return new WaitForSeconds(1.0f);
				debugConsole.GetComponent<DebugConsole>().log("CPU Usage: [" + iteration + "/5] " + perfCounter.NextValue() + "%");
			}
		}

		public IEnumerator getAvailableRAM(){
			perfCounter = new System.Diagnostics.PerformanceCounter();
			perfCounter.CategoryName = "Memory";
			perfCounter.InstanceName = "Available MBytes";
			perfCounter.NextValue();
			for (int iteration = 0; iteration < 5; ++iteration) {
				yield return new WaitForSeconds(1.0f);
				debugConsole.GetComponent<DebugConsole>().log("Available Ram: [" + iteration + "/5] " + perfCounter.NextValue() + "MB");
			}
		}
	}
}
