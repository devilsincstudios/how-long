/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

// This needs to be assigned to an object so it will be ran but it does not update any objects.
// To use this, do object.getComponent<FSPCounter>().getFPS(); and assign it to a float for rendering.
//

using UnityEngine;
using System.Collections;

namespace DI_Debug {
	public class FPSCounter : MonoBehaviour {
		private float interval;
		private float accumulatedTime;
		private float framesRendered;
		private float timeLeft;
		private bool counterActive;
		private float fps;

		public void Start() {
			interval = 0.5f;
			timeLeft = interval;
			enabled = true;
		}

		public void Update() {
			if (counterActive) {
				timeLeft -= Time.deltaTime;
				accumulatedTime += Time.timeScale / Time.deltaTime;
				++framesRendered;
				if (timeLeft <= 0.0f) {
					fps = accumulatedTime / framesRendered;
					timeLeft = interval;
					accumulatedTime = 0.0f;
					framesRendered = 0;
				}
			}
		}

		public void toggle() {
			counterActive = !counterActive;
			timeLeft = interval;
			accumulatedTime = 0.0f;
		}

		public float getFPS() {
			return fps;
		}
	}
}