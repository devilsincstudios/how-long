/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This class defines a debug command that will be used by the console.
*	See DebugCommands.cs for more info.
*
*/

using System;
using System.Collections.Generic;
using System.Collections;

namespace DI_Debug {
	public class DebugCommand {
		public string name;
		public string description;
		public Delegate functionCall;
		public List<DebugCommand> children;

		public DebugCommand() {
			children = new List<DebugCommand>();
			name = "";
			description = "";
		}
		public void setName(string input) {
			name = input;
		}
		public string getName() {
			return name;
		}

		public void setDescription (string input) {
			description = input;
		}
		public string getDescription() {
			return description;
		}

		public void setFunction(Delegate function) {
			functionCall = function;
		}
		public void runFunction() {
			if (functionCall != null) {
				functionCall.DynamicInvoke();
			}
		}

		public void addChild(DebugCommand child) {
			children.Add(child);
		}
		public List<DebugCommand> getChildren() {
			return children;
		}
	}
}
