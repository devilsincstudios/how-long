// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System.Collections;

namespace DI_Editor {
	[CustomEditor(typeof(DI_Game.DoorTrigger))]
	[CanEditMultipleObjects]
	public class DoorTriggerHelper : Editor {
		DI_Game.DoorTrigger doorTrigger;
		public void Awake() {
			doorTrigger = (DI_Game.DoorTrigger)target;
		}

		public override void OnInspectorGUI() {
			EditorGUIUtility.LookLikeControls();
			EditorGUILayout.BeginVertical();
			//====================================================================================================
			// Door Settings
			EditorGUILayout.BeginHorizontal();
			doorTrigger.doubleDoor = EditorGUILayout.Toggle("Double Door", doorTrigger.doubleDoor);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			doorTrigger.door = (GameObject) EditorGUILayout.ObjectField("Door", doorTrigger.door, typeof(GameObject), true);
			if (doorTrigger.door != null && doorTrigger.doorHandle == null) {
				try {
					doorTrigger.doorHandle = doorTrigger.door.GetComponent<DI_Game.DoorController>().doorHandle;
				}
				catch (MissingComponentException err) {
					Debug.LogException(err);
				}
			}
			EditorGUILayout.EndHorizontal();

			if (doorTrigger.doubleDoor) {
				EditorGUILayout.BeginHorizontal();
				doorTrigger.doorTwo = (GameObject) EditorGUILayout.ObjectField("Door Two", doorTrigger.doorTwo, typeof(GameObject), true);
				if (doorTrigger.doorTwo != null && doorTrigger.doorHandleTwo == null) {
					doorTrigger.doorHandleTwo = doorTrigger.doorTwo.GetComponent<DI_Game.DoorController>().doorHandle;
				}
				EditorGUILayout.EndHorizontal();
			}
			else {
				if (doorTrigger.doorTwo == null) {
					doorTrigger.doorTwo = GameObject.Find("Null Object");
					doorTrigger.doorHandleTwo = GameObject.Find("Null Object");
				}
			}
			//====================================================================================================
			EditorGUILayout.EndVertical();
		}
	}
}
