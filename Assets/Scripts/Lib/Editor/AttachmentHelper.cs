// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace DI_Editor
{
	[CustomEditor(typeof(DI_Game.AttachmentPoints))]
	public class AttachmentHelper : Editor
	{
		private DI_Game.AttachmentPoints attachmentPoints;
		private string[] typeNames;
		private GameObject bone;
		private List<DI_Game.AttachmentTypes> types;
		private Color defaultColor;
		private bool showRigEditor = false;

		public void SaveBones()
		{
			if (attachmentPoints.bonesIndex == null) {
				attachmentPoints.bonesIndex = new List<DI_Game.AttachmentTypes>();
				attachmentPoints.bonesValues = new List<GameObject>();
			}
			else {
				attachmentPoints.bonesIndex.Clear();
				attachmentPoints.bonesValues.Clear();
			}
			
			foreach (KeyValuePair<DI_Game.AttachmentTypes, GameObject> data in attachmentPoints.bones) {
				attachmentPoints.bonesIndex.Add(data.Key);
				attachmentPoints.bonesValues.Add(data.Value);
			}
			EditorUtility.SetDirty(target);
		}

		public void SerializeBones()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying) {
				SaveBones();
			}
		}

		public void DeSerialzeBones()
		{
			if (attachmentPoints.bonesIndex != null) {
				if (attachmentPoints.bones == null) {
					attachmentPoints.bones = new Dictionary<DI_Game.AttachmentTypes, GameObject>();
				}
				for (int index = 0; index < attachmentPoints.bonesIndex.Count; ++index) {
					setRigPoint(attachmentPoints.bonesIndex[index], attachmentPoints.bonesValues[index]);
				}
			}
		}

		public void OnEnable()
		{
			attachmentPoints = (DI_Game.AttachmentPoints)target;
			DeSerialzeBones();
			typeNames = Enum.GetNames(typeof(DI_Game.AttachmentTypes));
			types = new List<DI_Game.AttachmentTypes>();
			foreach (string type in typeNames) {
				types.Add((DI_Game.AttachmentTypes) Enum.Parse(typeof(DI_Game.AttachmentTypes), type));
			}
			defaultColor = GUI.color;
			if (attachmentPoints.rigNode == null) {
				attachmentPoints.rigNode = attachmentPoints.gameObject;
			}

			EditorApplication.playmodeStateChanged += SerializeBones;
		}
		
		public void setRigPoint(DI_Game.AttachmentTypes type, GameObject bone)
		{

			
			GameObject previousEntry;
			if (attachmentPoints.bones.TryGetValue(type, out previousEntry)) {
				attachmentPoints.bones[type] = bone;
			}
			else {
				attachmentPoints.bones.Add(type, bone);
			}
			attachmentPoints.setBone(type, bone);
		}

		public override void OnInspectorGUI()
		{
			EditorGUIUtility.LookLikeControls();
			EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal();
			showRigEditor = EditorGUILayout.Foldout(showRigEditor, "Rig Binding Editor");
			EditorGUILayout.EndHorizontal();

			if (showRigEditor) {
				EditorGUILayout.BeginVertical();
				int iteration = 0;
				foreach (DI_Game.AttachmentTypes attachmentPoint in types) {
					GameObject bone;
					if (!attachmentPoints.bones.TryGetValue(attachmentPoint, out bone)) {
						bone = attachmentPoints.gameObject;
					}
					EditorGUILayout.BeginHorizontal();
					if (bone.name == attachmentPoints.gameObject.name) {
						GUI.color = Color.red;
					}
					else {
						GUI.color = defaultColor;
					}
					bone = (GameObject) EditorGUILayout.ObjectField(typeNames[iteration], bone, typeof(GameObject), true);
					attachmentPoints.setBone(attachmentPoint, bone);
					EditorGUILayout.EndHorizontal();
					++iteration;
				}
				EditorGUILayout.EndVertical();
			}

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Separator();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			if (attachmentPoints.rigNode.name == attachmentPoints.gameObject.name) {
				GUI.color = Color.red;
			}
			else {
				GUI.color = defaultColor;
			}
			attachmentPoints.rigNode = (GameObject) EditorGUILayout.ObjectField("Character Rig Node", attachmentPoints.rigNode, typeof(GameObject), true);
			EditorGUILayout.EndHorizontal();

			GUI.color = defaultColor;

			EditorGUILayout.Separator();
			if (attachmentPoints.rigNode.name == attachmentPoints.gameObject.name) {
				EditorGUILayout.BeginHorizontal();
				GUI.color = Color.red;
				EditorGUILayout.LabelField("Notice: Not setting the rig node is likely to cause a bad auto map.");
				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			if (GUILayout.Button("Attempt Automapping")) {
				Transform[] children = attachmentPoints.rigNode.GetComponentsInChildren<Transform>();
				foreach (Transform node in children) {
					GameObject nodeObject = node.gameObject;
					string nodeName = node.name.ToLower();
					// Head
					if (nodeName.Contains("head")) {
						setRigPoint(DI_Game.AttachmentTypes.HEAD, nodeObject);
					}

					// Chest
					else if (nodeName.Contains("chest")) {
						setRigPoint(DI_Game.AttachmentTypes.CHEST, nodeObject);
					}

					// Neck
					else if (nodeName.Contains("neck")) {
						setRigPoint(DI_Game.AttachmentTypes.NECK, nodeObject);
					}

					// Pelvis
					else if (nodeName.Contains("pelvis")) {
						setRigPoint(DI_Game.AttachmentTypes.PELVIS, nodeObject);
					}

					// Shoulders
					else if (nodeName.Contains("collarbone_l")) {
						setRigPoint(DI_Game.AttachmentTypes.LEFT_SHOULDER, nodeObject);
					}

					else if (nodeName.Contains("collarbone_r")) {
						setRigPoint(DI_Game.AttachmentTypes.RIGHT_SHOULDER, nodeObject);
					}

					// Ankles
					else if (nodeName.Contains("ankle")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_ANKLE, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_ANKLE, nodeObject);
						}
					}

					// Hands
					else if (nodeName.Contains("palm")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_HAND, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_HAND, nodeObject);
						}
					}

					// Upper legs / arms
					else if (nodeName.Contains("upper")) {
						if (nodeName.Contains("leg")) {
							if (nodeName.Contains("_l")) {
								setRigPoint(DI_Game.AttachmentTypes.LEFT_LEG_UPPER, nodeObject);
							}
							else if (nodeName.Contains("_r")) {
								setRigPoint(DI_Game.AttachmentTypes.RIGHT_LEG_UPPER, nodeObject);
							}
						}
						if (nodeName.Contains("arm")) {
							if (nodeName.Contains("_l")) {
								setRigPoint(DI_Game.AttachmentTypes.LEFT_ARM_UPPER, nodeObject);
							}
							else if (nodeName.Contains("_r")) {
								setRigPoint(DI_Game.AttachmentTypes.RIGHT_ARM_UPPER, nodeObject);
							}
						}
					}
					// Lower legs / arms
					else if (nodeName.Contains("lower")) {
						if (nodeName.Contains("leg")) {
							if (nodeName.Contains("_l")) {
								setRigPoint(DI_Game.AttachmentTypes.LEFT_LEG_LOWER, nodeObject);
							}
							else if (nodeName.Contains("_r")) {
								setRigPoint(DI_Game.AttachmentTypes.RIGHT_LEG_LOWER, nodeObject);
							}
						}
						if (nodeName.Contains("arm")) {
							if (nodeName.Contains("_l")) {
								setRigPoint(DI_Game.AttachmentTypes.LEFT_ARM_LOWER, nodeObject);
							}
							else if (nodeName.Contains("_r")) {
								setRigPoint(DI_Game.AttachmentTypes.RIGHT_ARM_LOWER, nodeObject);
							}
						}
					}

					// New Karen - Toes
					else if (nodeName.Contains("toe_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_TOES, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_TOES, nodeObject);
						}
					}

					// New Karen - Feet
					else if (nodeName.Contains("foot_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_FOOT, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_FOOT, nodeObject);
						}
					}

					// New Karen - Lower Leg
					else if (nodeName.Contains("shin_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_LEG_LOWER, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_LEG_LOWER, nodeObject);
						}
					}
					// New Karen - Upper Leg
					else if (nodeName.Contains("thigh_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_LEG_UPPER, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_LEG_UPPER, nodeObject);
						}
					}
					// New Karen - Shoulder
					else if (nodeName.Contains("clavicle_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_SHOULDER, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_SHOULDER, nodeObject);
						}
					}
					// New Karen - Lower Arms
					else if (nodeName.Contains("forearm_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_ARM_LOWER, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_ARM_LOWER, nodeObject);
						}
					}
					// New Karen - Pevlis
					else if (nodeName.Equals("hips")) {
						setRigPoint(DI_Game.AttachmentTypes.PELVIS, nodeObject);
					}

					// New Karen - Hands
					else if (nodeName.Contains("hand_")) {
						if (nodeName.Contains("_l")) {
							setRigPoint(DI_Game.AttachmentTypes.LEFT_HAND, nodeObject);
						}
						else if (nodeName.Contains("_r")) {
							setRigPoint(DI_Game.AttachmentTypes.RIGHT_HAND, nodeObject);
						}
					}
				}
				SaveBones();
			}
			GUI.color = defaultColor;
			EditorGUILayout.Space();
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();
		}
	}
}