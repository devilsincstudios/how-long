// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace DI_Editor {
	[CustomEditor(typeof(DI_Game.Player))]
	public class PlayerEditor : Editor {
		/*
		 * 
		 * --- Actor
		 * public float currentHealth;
		 * public float maxHealth;
		 * public List<StatusEffect> statusEffects;
		 * public int inventoryId;
		 * public GameObject avatar;
		 * public float maxWeight;
		 * public float currentWeight;
		 * private float timeDelta;
		 * protected DI_Item.Inventory inventory;
		 * --- Player
		 * public PlayerType playerType;
		 * public float currentRest;
		 * public float maxRest;
		 */

		float MAXSTATUSEFFECTDURATION;
		DI_Game.Player player;
		List<int> inventoryIds;
		List<string> inventoryNames;
		List<int> inventoryCount;
		Vector2 statusEffectPosition;
		bool showStatusEffects = false;

		public void Awake() {
			player = (DI_Game.Player)target;
			inventoryCount = new List<int>();
			statusEffectPosition = new Vector2();

			inventoryIds = DI_Item.InventoryManager.getInventoryIds();
			if (inventoryIds.Count != 0) {
				inventoryNames = DI_Item.InventoryManager.getInventoryNames();
				List<string> temp = new List<string>();
				for (int iteration = 0; iteration < inventoryIds.Count; ++iteration) {
					temp.Add(inventoryIds[iteration] + ": " + inventoryNames[iteration]);
				}
				inventoryNames = temp;
				inventoryCount.Clear();
				for (int iteration = 0; iteration < inventoryIds.Count; ++iteration) {
					inventoryCount.Add(iteration + 1);
				}
			}
		}

		public override void OnInspectorGUI() {
			EditorGUIUtility.LookLikeControls();

			// Player Type
			EditorGUILayout.BeginHorizontal();
			player.playerType = (DI_Game.PlayerType) EditorGUILayout.EnumPopup("Player Type:", player.playerType, GUILayout.ExpandWidth(true), GUILayout.Width(250));
			EditorGUILayout.EndHorizontal();

			// Health
			EditorGUILayout.BeginVertical();
			player.maxHealth = EditorGUILayout.IntField("Max Health:", player.maxHealth);
			player.currentHealth = EditorGUILayout.IntSlider("Current Health:", player.currentHealth, 0, player.maxHealth);
			EditorGUILayout.EndVertical();

			// Rest
			EditorGUILayout.BeginVertical();
			player.maxRest = EditorGUILayout.IntField("Max Rest:", player.maxRest);
			player.currentRest = EditorGUILayout.IntSlider("Current Rest:", player.currentRest, 0, player.maxRest);
			EditorGUILayout.EndVertical();

			// Weight
			EditorGUILayout.BeginVertical();
			player.maxWeight = EditorGUILayout.FloatField("Max Weight:", player.maxWeight);
			EditorGUILayout.LabelField("Current Weight: " + player.currentWeight);
			EditorGUILayout.EndVertical();

			// Inventory
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Inventory:", GUILayout.Width(70));
			player.inventoryId = EditorGUILayout.IntPopup(player.inventoryId, inventoryNames.ToArray(), inventoryIds.ToArray(), GUILayout.ExpandWidth(false), GUILayout.Width(250));
			if (GUI.changed) {
				DI_Item.Inventory _tempInventory = DI_Item.InventoryManager.loadInventory(player.inventoryId);
				player.currentWeight = _tempInventory.currentWeight;
			}
			EditorGUILayout.EndHorizontal();

			// Avatar
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Avatar:", GUILayout.Width(50));
			player.avatar = (GameObject) EditorGUILayout.ObjectField(player.avatar, typeof(GameObject), true);
			EditorGUILayout.EndHorizontal();

			// Status Effects
			showStatusEffects = EditorGUILayout.Foldout(showStatusEffects, "Status Effects");
			if (showStatusEffects) {
				EditorGUILayout.BeginHorizontal();
				statusEffectPosition = EditorGUILayout.BeginScrollView(statusEffectPosition);
				int statusEffectIndex = 0;
				try {
					for (statusEffectIndex = 0; statusEffectIndex < player.statusEffects.Count; ++statusEffectIndex) {
						bool removedEntry = false;
						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.LabelField("Status Effect:", GUILayout.Width(100));
						player.statusEffects[statusEffectIndex].status = (DI_Game.StatusTypes) EditorGUILayout.EnumPopup(player.statusEffects[statusEffectIndex].status, GUILayout.ExpandWidth(false));
						if (GUILayout.Button("X", new GUIStyle("button"), GUILayout.ExpandWidth(false))) {
							player.statusEffects.RemoveAt(statusEffectIndex);
							removedEntry = true;
						}
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.BeginHorizontal();
						if (removedEntry == false) {
							player.statusEffects[statusEffectIndex].timeLeft = EditorGUILayout.Slider(player.statusEffects[statusEffectIndex].timeLeft, 0, DI_Game.StatusLib.getMaxEffectDuration());
						}
						EditorGUILayout.EndHorizontal();
					}
				}
				catch (NullReferenceException err) {
					Debug.LogException(err);
					player.statusEffects = new List<DI_Game.StatusEffect>();
				}
				catch (Exception err) {
					Debug.LogException(err);
				}
				EditorGUILayout.EndScrollView();
				EditorGUILayout.EndHorizontal();

				EditorGUILayout.BeginHorizontal();
				if (GUILayout.Button("Add", GUILayout.ExpandWidth(false))) {
					player.statusEffects.Add(new DI_Game.StatusEffect());
				}
				EditorGUILayout.EndHorizontal();
			}
			EditorUtility.SetDirty(player);
		}
	}
}
