/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for types of weather conditions
*
*/

namespace DI_Weather {
	public enum WeatherTypes {
		CLEAR,
		RAIN,
		OVERCAST,
		STORM,
		FOG
	}
}
