// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_Game {
	public enum Materials {
		CONCRETE,
		DIRT,
		GENERIC,
		METAL,
		WOOD
	};
}