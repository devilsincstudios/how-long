// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace DI_Game {
	public static class MaterialSounds {
		static Dictionary<Materials, MaterialSound> materials = new Dictionary<Materials, MaterialSound>();
		static bool soundsLoaded = false;

		public static void loadSounds() {
			// These objects are only for hierarchy view reasons
			GameObject sounds = new GameObject("Sounds");
			GameObject footsteps = new GameObject("Footsteps");
			footsteps.transform.parent = sounds.transform.parent;
			sounds.transform.parent = GameObject.Find("Environment").transform;

			// Loop though the materials enum and load the sounds tied to them.
			foreach (string materialName in Enum.GetNames(typeof(Materials))) {
				GameObject sound = new GameObject(materialName);
				sound.AddComponent<MaterialSound>();
				MaterialSound materialSound = sound.GetComponent<MaterialSound>();
				AudioClip[] audioClips = Resources.LoadAll<AudioClip>("Sounds/Footsteps/" + materialName);
				materialSound.sounds = new List<AudioClip>(audioClips);
				materialSound.materialType = (Materials) Enum.Parse(typeof(Materials), materialName);
				sound.tag = "Footsteps_" + materialName;
				sound.transform.parent = footsteps.transform;
				materials.Add((Materials) Enum.Parse(typeof(Materials), materialName), materialSound);
			}
			soundsLoaded = true;
		}

		public static MaterialSound getSounds(Materials material) {
			if (!soundsLoaded) {
				loadSounds();
			}
			return materials[material];
		}
	}
}
