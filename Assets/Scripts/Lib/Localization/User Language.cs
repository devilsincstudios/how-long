﻿using UnityEngine;
using System.Collections;

/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	The different languages strings will be available in
*
*/

namespace DI_Language
{

	public enum UserLanguage
	{
		FRENCH, 
		ENGLISH, 
		GERMAN, 
		SPANISH,
		ITALIAN,
		PORTUGUESE
	};
}
