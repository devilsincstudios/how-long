// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_Item
{
	public enum AmmoType
	{
		_9MM,
		_10MM,
		_3_75MM,
		_45MM,
		_50MM,
		_UNASSIGNED,
	}
}