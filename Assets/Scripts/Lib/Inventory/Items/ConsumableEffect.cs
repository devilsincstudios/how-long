// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_Item
{
	public enum ConsumableEffect
	{
		_BUFF,
		_CURE,
		_HEAL_BOTH,
		_HEAL_HP,
		_HEAL_STAM,
		_UNASSIGNED,
	}
}