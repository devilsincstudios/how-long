// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

namespace DI_Item
{
	[Serializable]
	[XmlRoot("Inventory")]
	public class Inventory
	{
		[XmlAttribute("Inventory Data")]
		public int inventoryId;
		public List<Item> inventoryItems;
		public int maxSlots = 1;
		public int slotsUsed = 0;
		public float currentWeight = 0.0f;
		public string inventoryName = "Inventory";
		public string inventoryDescription = "Inventory";
		public InventoryType type = InventoryType.LOOT;

		public Inventory() {
			inventoryId = InventoryManager.getNextInventoryId();
			inventoryItems = new List<Item>();
		}

		public Inventory(int id)
		{
			Inventory _inventory = InventoryManager.loadInventory(id);
			maxSlots = _inventory.maxSlots;
			slotsUsed = _inventory.slotsUsed;
			inventoryItems = _inventory.inventoryItems;
			currentWeight = _inventory.currentWeight;
		}

		public int getId()
		{
			return inventoryId;
		}

		public List<Item> getItems()
		{
			return inventoryItems;
		}

		public int getMaxSlots()
		{
			return maxSlots;
		}

		public int getUsedSlots()
		{
			return slotsUsed;
		}

		public float getWeight()
		{
			return currentWeight;
		}

		public Item getItem(int slot)
		{
			if (inventoryItems.Count > slot && inventoryItems[slot] != null) {
				return inventoryItems[slot];
			}
			else {
				return new Item();
			}
		}

		public void calculateWeight()
		{
			float weight = 0.0f;
			foreach (Item _item in inventoryItems.ToArray()) {
				weight += _item.weight * _item.stackSize;
			}
			currentWeight = weight;
		}

		public bool moveItem(int slotOne, int slotTwo)
		{
			Item tempItemOne = inventoryItems[slotOne];
			removeItem(slotOne);
			addItem(tempItemOne, slotTwo);
			DI_Events.EventCenter<float, int, DI_Events.InventoryEventType>.invoke("onInventoryUpdate", currentWeight, slotsUsed, DI_Events.InventoryEventType.MOVE);
			Debug.Log("Inventory Update: Move");
			return true;
		}

		public bool swapItems(int slotOne, int slotTwo)
		{
			Item tempItemOne = inventoryItems[slotOne];
			Item tempItemTwo = inventoryItems[slotTwo];
			inventoryItems[slotOne] = tempItemTwo;
			inventoryItems[slotTwo] = tempItemOne;
			DI_Events.EventCenter<float, int, DI_Events.InventoryEventType>.invoke("onInventoryUpdate", currentWeight, slotsUsed, DI_Events.InventoryEventType.SWAP);
			Debug.Log("Inventory Update: Swap");
			return true;
		}

		public bool setItem(Item item, int slot)
		{
			Debug.Log("Attempting set inventory - Slot:" + slot + " name: " + item.itemName + " slots: " + item.slots + " weight: " + item.weight);
			Debug.Log("Would be new slots: " + (item.slots + slotsUsed) + " max slots: " + maxSlots);
			try {
				if (inventoryItems[slot].itemId != 0) {
					removeItem(slot);
				}
			}
			catch (ArgumentOutOfRangeException) {
			}

			if (item.slots + slotsUsed <= maxSlots) {
				return addItem(item, slot);
			}
			else {
				Debug.Log("Not enough slots: Set");
				return false;
			}
		}

		public bool addItem(Item item, int slot) {
			int freeSlots = maxSlots - slotsUsed;
			Debug.Log("Attempting add inventory - Slot:" + slot + " name: " + item.itemName + " slots: " + item.slots + " weight: " + item.weight);
			try {
				removeItem(slot);
			}
			catch (Exception){
			}

			if (item.stackable) {
				foreach (Item storedItem in inventoryItems.ToArray()) {
					if (item.itemId == storedItem.itemId) {
						if (item.stackSize + storedItem.stackSize < item.maxStackSize) {
							storedItem.stackSize += item.stackSize;
							slotsUsed += item.slots;
							calculateWeight();
							DI_Events.EventCenter<float, int, DI_Events.InventoryEventType>.invoke("onInventoryUpdate", currentWeight, slotsUsed, DI_Events.InventoryEventType.ADD);
							Debug.Log("Inventory Update: Add");
							return true;
						}
						else {
							item.stackSize -= storedItem.maxStackSize - storedItem.stackSize;
							storedItem.stackSize = storedItem.maxStackSize;
							slotsUsed += item.slots;
							calculateWeight();
							DI_Events.EventCenter<float, int, DI_Events.InventoryEventType>.invoke("onInventoryUpdate", currentWeight, slotsUsed, DI_Events.InventoryEventType.ADD);
							Debug.Log("Inventory Update: Add");
							if (item.slots > freeSlots) {
								throw new DI_Exceptions.InventoryException(item, "Only able to add part of a stackable item");
							}
						}
					}
				}
			}

			if (item.slots > freeSlots) {
				return false;
			}
			else {
				slotsUsed += item.slots;
				try {
					inventoryItems[slot] = item;
				}
				catch (Exception){
					inventoryItems.Insert(slot, item);
				}

				calculateWeight();
				DI_Events.EventCenter<float, int, DI_Events.InventoryEventType>.invoke("onInventoryUpdate", currentWeight, slotsUsed, DI_Events.InventoryEventType.ADD);
				Debug.Log("Inventory Update: Add");
				return true;
			}
		}

		public void removeItem(int slot) {
			try {
				slotsUsed -= inventoryItems[slot].slots;
				inventoryItems.RemoveAt(slot);
				calculateWeight();
				DI_Events.EventCenter<float, int, DI_Events.InventoryEventType>.invoke("onInventoryUpdate", currentWeight, slotsUsed, DI_Events.InventoryEventType.REMOVE);
				Debug.Log("Inventory Update: Remove");
			}
			catch (ArgumentOutOfRangeException) {
			}
		}
	}
}
