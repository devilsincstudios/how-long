// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_GUI
{
	public static class InventoryDragHelper
	{
		public static DI_Item.Item _item;
		public static int _slot;
		public static DI_Item.Inventory _inventory;
		public static DI_GUI.GUIId _guiId;

		public static void setDrag(DI_Item.Item item, int slot, DI_Item.Inventory inventory, DI_GUI.GUIId guiId)
		{
			_item = item;
			_slot = slot;
			_inventory = inventory;
			_guiId = guiId;
		}

		public static DI_Item.Item getItem()
		{
			return _item;
		}

		public static int getSlot()
		{
			return _slot;
		}

		public static DI_Item.Inventory getInventory()
		{
			return _inventory;
		}
	}
}