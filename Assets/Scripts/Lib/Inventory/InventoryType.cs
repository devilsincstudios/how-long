// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_Item
{
	public enum InventoryType
	{
		PLAYER,
		HOSTILE_NPC,
		FRIENDLY_NPC,
		SHOP,
		LOOT,
	}
}