// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	Static class to manage key bindings for each player.
// *
// */

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Input {
	public static class BindManager {
		private static Dictionary<RegisteredKeys, string> p1boundKeys = new Dictionary<RegisteredKeys, string>();
		private static Dictionary<RegisteredKeys, BindType> p1boundKeyTypes = new Dictionary<RegisteredKeys, BindType>();
		private static Dictionary<RegisteredKeys, string> p2boundKeys = new Dictionary<RegisteredKeys, string>();
		private static Dictionary<RegisteredKeys, BindType> p2boundKeyTypes = new Dictionary<RegisteredKeys, BindType>();

		// Get the currently bound keys for <player>
		public static Dictionary<RegisteredKeys, string> getBinds(int player) {
			if (player == 1) {
				return p1boundKeys;
			}
			else {
				return p2boundKeys;
			}
		}

		// Wrapper to setKey that also sets the type of the key.
		//=================================================================================================
		public static void bindKey(RegisteredKeys key, string axis, int player) {
			DI_Debug.Logger.Instance.simpleLog("Info", "System", "Binding: " + Enum.GetName(typeof(RegisteredKeys), key) + " to axis: " + axis + " for player: " + player);
			setKey(key, axis, player);
			setType(key, BindType.BIND_AXIS, player);
		}
		public static void bindKey(RegisteredKeys key, KeyCode code, int player) {
			DI_Debug.Logger.Instance.simpleLog("Info", "System", "Binding: " + Enum.GetName(typeof(RegisteredKeys), key) + " to key: " + Enum.GetName(typeof(KeyCode), code) + " for player: " + player);
			setKey(key, Enum.GetName(typeof(KeyCode), code), player);
			setType(key, BindType.BIND_KEY, player);
		}
		//=================================================================================================

		// Returns the value bound to the key specificed for the player.
		// Example RegisteredKeys.FIRE = KeyCode.Mouse1
		public static string getKey(RegisteredKeys key, int player) {
			string boundKey;
			if (player == 1) {
				if (p1boundKeys.TryGetValue(key, out boundKey)) {
					return boundKey;
				}
				else {
					throw new Exception("No key is bound to that function. " + Enum.GetName(typeof(RegisteredKeys), key) + " for player: " + player);
				}
			}
			else {
				if (p2boundKeys.TryGetValue(key, out boundKey)) {
					return boundKey;
				}
				else {
					throw new Exception("No key is bound to that function. " + Enum.GetName(typeof(RegisteredKeys), key) + " for player: " + player);
				}
			}
		}

		// Sets the registed key to a keycode / axis value for the player.
		// Example Mouse1 = RegisteredKeys.FIRE
		public static void setKey(RegisteredKeys key, string value, int player) {
			string previous;
			if (player == 1) {
				if (p1boundKeys.TryGetValue(key, out previous)) {
					p1boundKeys[key] = value;
				}
				else {
					p1boundKeys.Add(key, value);
				}
			}
			else {
				if (p2boundKeys.TryGetValue(key, out previous)) {
					p2boundKeys[key] = value;
				}
				else {
					p2boundKeys.Add(key, value);
				}
			}
		}

		// Returns the type of bind that is being used for the key.
		// Returns either KEY, AXIS, or UNDEFINED.
		public static BindType getType(RegisteredKeys key, int player) {
			BindType type;
			if (player == 1) {
				if (p1boundKeyTypes.TryGetValue(key, out type)) {
					return p1boundKeyTypes[key];
				}
				else {
					return BindType.UNDEFINED;
				}
			}
			else {
				if (p2boundKeyTypes.TryGetValue(key, out type)) {
					return p2boundKeyTypes[key];
				}
				else {
					return BindType.UNDEFINED;
				}
			}
		}

		// Sets the type of bind that is being used for the key.
		// KEY, AXIS, or UNDEFINED.
		public static void setType(RegisteredKeys key, BindType type, int player) {
			BindType bindType;
			if (player == 1) {
				if (p1boundKeyTypes.TryGetValue(key, out bindType)) {
					p1boundKeyTypes[key] = type;
				}
				else {
					p1boundKeyTypes.Add(key, type);
				}
			}
			else {
				if (p2boundKeyTypes.TryGetValue(key, out bindType)) {
					p2boundKeyTypes[key] = type;
				}
				else {
					p2boundKeyTypes.Add(key, type);
				}
			}
		}
	}
}