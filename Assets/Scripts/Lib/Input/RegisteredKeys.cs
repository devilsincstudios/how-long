/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for bindable keys aka game functions.
*
*/

namespace DI_Input {
	public enum RegisteredKeys {
		AIM,
		ATTACK,
		BACKWARDS,
		CAMERA_X,
		CAMERA_Y,
		CROUCH,
		DEV_CONSOLE,
		DEV_MENU,
		DEV_EXIT,
		FLASHLIGHT,
		FORWARDS,
		HORIZONTAL,
		INTERACT,
		INVENTORY,
		JOURNAL,
		JUMP,
		MAP,
		NEXT_WEAPON,
		PREVIOUS_WEAPON,
		QUICK_HEAL,
		RESET_CAMERA,
		SPRINT,
		TOGGLE_STEALTH,
		TURN_LEFT,
		TURN_RIGHT,
		UNBOUND,
		VIEW_PDA,
		WEAPON_SWAP,
		VERTICAL
	}
}