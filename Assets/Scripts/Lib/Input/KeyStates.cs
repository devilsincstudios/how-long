/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for key states
*
*/

namespace DI_Input {
	public enum KeyStates {
		KEY_NEGATIVE,
		KEY_POSITIVE,
		KEY_PRESSED,
		KEY_NOT_PRESSED
	}
}