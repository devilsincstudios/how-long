// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	ENUM values used for key bindings.
// *
// */

namespace DI_Input {
	public enum ControllerType {
		PLAYER_ONE_KEYBOARD,
		PLAYER_TWO_KEYBOARD,
		PLAYER_ONE_CONTROLLER_ONE,
		PLAYER_TWO_CONTROLLER_ONE,
		PLAYER_TWO_CONTROLLER_TWO,
		CONTROLLER_NONE
	}
}
