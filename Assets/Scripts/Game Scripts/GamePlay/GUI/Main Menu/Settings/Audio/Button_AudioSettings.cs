// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;
using DI_Audio;

namespace DI_Game
{
	[AddComponentMenu("Menus/Main/Settings/Audio")]
	public class Button_AudioSettings : MenuItem
	{
		public bool increase;
		public AudioTypes type;

		public int backgroundMusicLevel;
		public int soundFXLevel;
		public int voiceLevel;
		public GameObject displayValue;

		new public void OnEnable() {
			if (type == AudioTypes.BACKGROUND_MUSIC) {
				backgroundMusicLevel = PlayerPrefs.GetInt("Audio/Background Music Level", 10);
				displayValue.GetComponent<TextMesh>().text = backgroundMusicLevel.ToString();
			}
			if (type == AudioTypes.SOUND_FX) {
				soundFXLevel = PlayerPrefs.GetInt("Audio/Sound FX Level", 10);
				displayValue.GetComponent<TextMesh>().text = soundFXLevel.ToString();
			}
			if (type == AudioTypes.VOICE) {
				voiceLevel = PlayerPrefs.GetInt("Audio/Voice Level", 10);
				displayValue.GetComponent<TextMesh>().text = voiceLevel.ToString();
			}
			base.OnEnable();
		}

		override public void clickAction() {
			Debug.Log ("clickAction");
			if (type == AudioTypes.BACKGROUND_MUSIC) {
				backgroundMusicLevel = PlayerPrefs.GetInt("Audio/Background Music Level", 10);
				if (increase) {
					if (backgroundMusicLevel < 10) {
						++backgroundMusicLevel;
					}
				}
				else {
					if (backgroundMusicLevel > 1) {
						--backgroundMusicLevel;
					}
				}

				PlayerPrefs.SetInt("Audio/Background Music Level", backgroundMusicLevel);
				displayValue.GetComponent<TextMesh>().text = backgroundMusicLevel.ToString();
			}

			if (type == AudioTypes.SOUND_FX) {
				soundFXLevel = PlayerPrefs.GetInt("Audio/Sound FX Level", 10);
				if (increase) {
					if (soundFXLevel < 10) {
						++soundFXLevel;
					}
				}
				else {
					if (soundFXLevel > 1) {
						--soundFXLevel;
					}
				}
				
				PlayerPrefs.SetInt("Audio/Sound FX Level", soundFXLevel);
				displayValue.GetComponent<TextMesh>().text = soundFXLevel.ToString();
			}

			if (type == AudioTypes.VOICE) {
				voiceLevel = PlayerPrefs.GetInt("Audio/Voice Level", 10);
				if (increase) {
					if (voiceLevel < 10) {
						++voiceLevel;
					}
				}
				else {
					if (voiceLevel > 1) {
						--voiceLevel;
					}
				}
				
				PlayerPrefs.SetInt("Audio/Voice Level", voiceLevel);
				displayValue.GetComponent<TextMesh>().text = voiceLevel.ToString();
			}
			this.stopHover();
		}
	}
}