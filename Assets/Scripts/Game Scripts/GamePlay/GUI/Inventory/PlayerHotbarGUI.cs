/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_GUI {
	[AddComponentMenu("GUI/Inventory/PlayerHotbarGUI")]
	public class PlayerHotbarGUI : MonoBehaviour {
		
		public int inventoryId;
		private DI_Item.Inventory inventory;
		
		public void OnEnable()
		{
			inventory = DI_Item.InventoryManager.loadInventory(inventoryId);
		}
		
		public void OnGUI()
		{
			
		}
	}
}
