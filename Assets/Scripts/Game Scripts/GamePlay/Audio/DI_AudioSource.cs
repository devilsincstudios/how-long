// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

// Abandoning this for now.
// Will revisit later
// This was supposed to apply a gobal multiplier on the volume of all sound clips based on the type of sound.
// Current implimentation will not work because AudioSource is a sealed class.

//using UnityEngine;
//namespace DI_Audio
//{
//	[AddComponentMenu("Audio/Audio Source")]
//	public class DI_AudioSource : AudioSource
//	{
//		public AudioTypes type;
//		private int volumeMultiplier;
//
//		public DI_AudioSource() {
//			switch (type) {
//				case AudioTypes.BACKGROUND_MUSIC:
//					volumeMultiplier = PlayerPrefs.GetInt("Audio/Background Music Level", 10);
//				break;
//				case AudioTypes.SOUND_FX:
//					volumeMultiplier = PlayerPrefs.GetInt("Audio/Sound FX Level", 10);
//				break;
//				case AudioTypes.VOICE:
//					volumeMultiplier = PlayerPrefs.GetInt("Audio/Voice Level", 10);
//				break;
//			}
//
//			volume = (volume / 10) * volumeMultiplier;
//		}
//	}
//}