// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	This class controlls how doors function.
// *
// */

using UnityEngine;

namespace DI_Game {
	[AddComponentMenu("Interactions/Doors/Door Controller")]
	public class DoorController : MonoBehaviour {
		public float closedX;
		public float closedY;
		public float closedZ;
		public float openX;
		public float openY;
		public float openZ;
		public float movementSpeed = 0.01f;
		public AudioSource openingSound;
		public AudioSource closingSound;
		public bool isOpen = false;
		public bool isSliding = false;

		// Mechanim helper object
		public GameObject doorHandle;

		private bool xLocked = false;
		private bool yLocked = false;
		private bool zLocked = false;

		private float currentX;
		private float currentY;
		private float currentZ;

		[System.NonSerialized]
		public bool isOpening;
		[System.NonSerialized]
		public bool isClosing;

		public void Start() {
			if (isOpen) {
				currentX = openX;
				currentY = openY;
				currentZ = openZ;
			}
			else {
				currentX = closedX;
				currentY = closedY;
				currentZ = closedZ;
			}

			if (closedX == openX) {
				xLocked = true;
			}
			if (closedY == openY) {
				yLocked = true;
			}
			if (closedZ == openZ) {
				zLocked = true;
			}

			isOpening = false;
			isClosing = false;
		}

		public void FixedUpdate() {
			if (isOpening) {
				moveDoor(openX, openY, openZ, DoorMovementType.OPEN);
			}
			if (isClosing) {
				moveDoor(closedX, closedY, closedZ, DoorMovementType.CLOSE);
			}
		}

		public void openDoor() {
			if (!isOpening) {
				isOpening = true;
				if (!openingSound.isPlaying) {
					openingSound.Play();
				}
			}
		}

		public void closeDoor() {
			if (!isClosing) {
				isClosing = true;
				if (!closingSound.isPlaying) {
					closingSound.Play();
				}
			}
		}

		// Move between starting point and given point.
		// If we are finished, let fixed update know.
		public void moveDoor(float x, float y, float z, DoorMovementType type) {
			//Debug.LogWarning("Moving Door.");
			//Debug.LogWarning("Current Position: " + currentX + ", " + currentY + ", " + currentZ);
			if (isSliding) {
				if (!xLocked) {
					currentX = Mathf.MoveTowards(currentX, x, movementSpeed / Time.deltaTime);
				}
				if (!yLocked) {
					currentY = Mathf.MoveTowards(currentY, y, movementSpeed / Time.deltaTime);
				}
				if (!zLocked) {
					currentZ = Mathf.MoveTowards(currentZ, z, movementSpeed / Time.deltaTime);
				}
			}
			else {
				// Attempt to correct negative angles not being handled well by movetowards.
				// Only alter values that make sense to change.
				if (!xLocked) {
					currentX = MathLib.wrapAngle(currentX);
					currentX = Mathf.MoveTowardsAngle(currentX, x, movementSpeed / Time.deltaTime);
				}
				if (!yLocked) {
					currentY = MathLib.wrapAngle(currentY);
					currentY = Mathf.MoveTowardsAngle(currentY, y, movementSpeed / Time.deltaTime);
				}
				if (!zLocked) {
					currentZ = MathLib.wrapAngle(currentZ);
					currentZ = Mathf.MoveTowardsAngle(currentZ, z, movementSpeed / Time.deltaTime);
				}
			}
			//Debug.LogWarning("Altered Position: " + currentX + ", " + currentY + ", " + currentZ);

			if (isSliding) {
				// Fudge the numbers a bit to prevent it from never reaching the target.
				if (Mathf.Abs(currentX - x) < 0.01f) {
					if (type == DoorMovementType.OPEN) {
						currentX = openX;
					}
					else {
						currentX = closedX;
					}
				}
				if (Mathf.Abs(currentY - y) < 0.01f) {
					if (type == DoorMovementType.OPEN) {
						currentY = openY;
					}
					else {
						currentY = closedY;
					}
				}
				if (Mathf.Abs(currentZ - z) < 0.01f) {
					if (type == DoorMovementType.OPEN) {
						currentZ = openZ;
					}
					else {
						currentZ = closedZ;
					}
				}

				if (currentX == x && currentY == y && currentZ == z) {
					if (type == DoorMovementType.OPEN) {
						isOpening = false;
						isOpen = true;
					}
					else if (type == DoorMovementType.CLOSE) {
						isClosing = false;
						isOpen = false;
					}
				}

				transform.localPosition = new Vector3(currentX, currentY, currentZ);
			}
			else {
				// Fudge the numbers a bit to prevent it from never reaching the target.
				if (Mathf.Abs(currentX - x) < 1) {
					if (type == DoorMovementType.OPEN) {
						currentX = openX;
					}
					else {
						currentX = closedX;
					}
				}
				if (Mathf.Abs(currentY - y) < 1) {
					if (type == DoorMovementType.OPEN) {
						currentY = openY;
					}
					else {
						currentY = closedY;
					}
				}
				if (Mathf.Abs(currentZ - z) < 1) {
					if (type == DoorMovementType.OPEN) {
						currentZ = openZ;
					}
					else {
						currentZ = closedZ;
					}
				}

				// If the current values are the same as the target values we are finished.
				if (currentX == x && currentY == y && currentZ == z) {
					if (type == DoorMovementType.OPEN) {
						isOpening = false;
						//Debug.LogWarning("Door is now open.");
						isOpen = true;
					}
					else if (type == DoorMovementType.CLOSE) {
						isClosing = false;
						//Debug.LogWarning("Door is now closed.");
						isOpen = false;
					}
				}

				// Update the doors rotation values.
				transform.localEulerAngles = new Vector3(currentX, currentY, currentZ);
			}
		}
	}
}
