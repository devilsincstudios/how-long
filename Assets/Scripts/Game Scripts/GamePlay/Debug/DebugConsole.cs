/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This is the debug console.
*	Entering/Selecting commands runs them against the game to help us with debugging.
*
*/

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DI_Debug {
	[AddComponentMenu("Debug/Console/Debug Console")]
	public class DebugConsole : UnityEngine.MonoBehaviour {
		
		private bool isEnabled;
		private List<string> logContents;
		
		private float width = Screen.width / 2;
		private float height = 200.0f;
		private Rect consoleDimensions;
		private Rect consoleArea;
		private Rect buttonLocation;
		private string logContentsString;
		private Rect autoComplete;
		private DebugCommands commandsObject;
		private List<DI_Debug.DebugCommand> rootNode;
		private string expanded;

		// MonoBehaviour inherits
		//=======================================================================================================================
		void Start() {
			logContents = new List<string>();
			isEnabled = false;
			consoleDimensions  = new Rect(5, 0, width, height);
			consoleArea = new Rect(5, 20, width, height - 40);
			logContentsString = "";
			commandsObject = new DebugCommands();
			rootNode = commandsObject.getCommands();
			expanded = "";
		}

		public void OnGUI() {
			if (isEnabled) {
				//TODO: Stylize this window.
				GUI.Window (0, consoleDimensions, DrawContents, "Developer Console - Pre-Alpha Build");
				DrawOutsideContents();
			}
		}
		//=======================================================================================================================

		//=======================================================================================================================
		// Writes a message to the consoles history log.
		//=======================================================================================================================
		public void log(string message) {
			message.Replace(Environment.NewLine, "");

			if (logContents.Count == 10) {
				logContents.RemoveAt(0);
			}
			
			logContents.Add(message);
			logContentsString = "";
			string [] logContentsArray = logContents.ToArray();
			for (int iteration = 0; iteration < logContentsArray.Length; ++iteration) {
				logContentsString = logContentsString + logContents[iteration] + "\n";
			}
		}
		//=======================================================================================================================

		//=======================================================================================================================
		// Clears the contents of the consoles history log.
		//=======================================================================================================================
		public void clearContents() {
			logContents.Clear();
			logContentsString = "";
		}
		//=======================================================================================================================

		//=======================================================================================================================
		// Toggles the visability of the console.
		//=======================================================================================================================
		public void toggle() {
			isEnabled = !isEnabled;
		}
		//=======================================================================================================================


		//=======================================================================================================================
		// Draws the contents of the console window.
		// NOTE: Everything that is supposed to be inside the window must be done in this call.
		//=======================================================================================================================
		private void DrawContents (int windowID) {
			// Draw any Controls inside the window here
			GUI.Label(consoleArea, logContentsString);
		}
		//=======================================================================================================================


		private bool isExpanded(DebugCommand command) {
			if (expanded == "") {
				return false;
			}

			if (command.name == expanded) {
				return true;
			}
			else {
				if (command.children.Count > 0) {
					foreach (DebugCommand child in command.getChildren().ToArray()) {
						if (isExpanded(child)) {
							return true;
						}
					}
				}
				else {
					return false;
				}
			}
			return false;
		}

		private void addCommandNode(DebugCommand command, int depth, int buttonNumber, Rect dimensions) {
			Rect button = new Rect(dimensions.x + 10 * depth, dimensions.height + 16 * buttonNumber, dimensions.width - 10 * depth, 16);
			if (GUI.Button(button, new GUIContent(command.getName(), command.getDescription()))) {
				command.runFunction();
				expanded = command.getName();
			}
		}
		
		private int addCommandTree(DebugCommand command, int depth, int buttonNumber, Rect dimensions) {
			addCommandNode(command, depth, buttonNumber, dimensions);
			++buttonNumber;
			if (isExpanded(command)) {
				if (command.children.Count != 0) {
					foreach (DebugCommand child in command.children.ToArray()) {
						buttonNumber = addCommandTree(child, depth + 1, buttonNumber, dimensions);
					}
				}
			}
			return buttonNumber;
		}
		//=======================================================================================================================
		// Draws contents that are to be displayed outside of the console window.
		//=======================================================================================================================
		private void DrawOutsideContents () {
				//Debug.Log("id: " + GUIUtility.keyboardControl);

				int depth = 0;
				int buttonNumber = 0;

				foreach (DebugCommand command in rootNode.ToArray()) {
				buttonNumber = addCommandTree(command, depth, buttonNumber, consoleDimensions);
				}
		}
		//=======================================================================================================================

	}
}