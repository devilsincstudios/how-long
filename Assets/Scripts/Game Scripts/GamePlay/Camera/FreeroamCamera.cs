﻿/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Freeroam Camera
*
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DI_Camera {
	[AddComponentMenu("Cameras/Freeroam")]
	public class FreeroamCamera : MonoBehaviour {
		public GameObject parentObject;
		public float topSpeed = 20f;
		public float acceleration = 2f;
		public float mouseSensitivity = 3f;
		public int playerControlling = 1;

		public bool artMode = false;

		private float currentSpeed;
		private bool isActiveCamera;
		private bool isMoving;
		
		private float yaw = 0f;
		private float pitch = 0f;
		private DI_Game.GameState gameState;

		private  GameObject masterMind;
		private DI_Game.GameStateController GSC;

		void Start() {
			isActiveCamera = false;
			this.transform.parent = parentObject.transform;
			this.transform.position = parentObject.transform.position;
			if (artMode) {
				enableCamera();
			}

			currentSpeed = 0.0f;
			// Register gamestate change listener.
			DI_Events.EventCenter<DI_Game.GameState>.addListener("onGameStateChange", handleGameStateChange);
			masterMind = GameObject.FindGameObjectWithTag("Master Mind");
			GSC = masterMind.GetComponent<DI_Game.GameStateController>();
		}

		public void handleGameStateChange(DI_Game.GameState state) {
			gameState = state;
		}
		
		void FixedUpdate () {
			if (gameState == DI_Game.GameState.PLAYING || artMode) {
				if (isActiveCamera) {
					isMoving = false;
					if (GSC.getKeyState(DI_Input.RegisteredKeys.ATTACK, playerControlling) == DI_Input.KeyStates.KEY_PRESSED
					    || GSC.getKeyState(DI_Input.RegisteredKeys.ATTACK, playerControlling) == DI_Input.KeyStates.KEY_POSITIVE
					    || GSC.getKeyState(DI_Input.RegisteredKeys.AIM, playerControlling) == DI_Input.KeyStates.KEY_PRESSED
					    || GSC.getKeyState(DI_Input.RegisteredKeys.AIM, playerControlling) == DI_Input.KeyStates.KEY_POSITIVE
					    ) {
						// Have to poll for changes on axis values.
						// Can't get around it.
						float deltaX = DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_X, playerControlling) * mouseSensitivity;
						float deltaY = DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_Y, playerControlling) * mouseSensitivity;
						updateYaw(deltaX);
						updatePitch(-deltaY);
					}

					// Turbo boost! =]
					//Default c
					if (GSC.getKeyState(DI_Input.RegisteredKeys.NEXT_WEAPON, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						currentSpeed = topSpeed * 4;
					}

					// Forward default w
					if (GSC.getKeyState(DI_Input.RegisteredKeys.FORWARDS, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						isMoving = true;
						moveForward();
					}
					// Backward default s
					if (GSC.getKeyState(DI_Input.RegisteredKeys.BACKWARDS, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						isMoving = true;
						moveBackward();
					}
					// Right default d
					if (GSC.getKeyState(DI_Input.RegisteredKeys.TURN_RIGHT, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						isMoving = true;
						strafeRight();
					}
					// Left default a
					if (GSC.getKeyState(DI_Input.RegisteredKeys.TURN_LEFT, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						isMoving = true;
						strafeLeft();
					}
					// Up default space
					if (GSC.getKeyState(DI_Input.RegisteredKeys.JUMP, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						isMoving = true;
						moveUp();
					}
					// Down default z
					if (GSC.getKeyState(DI_Input.RegisteredKeys.PREVIOUS_WEAPON, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						isMoving = true;
						moveDown();
					}
					if (!isMoving) {
						currentSpeed = 0.0f;
					}
				}
			}
		}

		// Camera Control functions
		public bool isEnabled() {
			return isActiveCamera;
		}
		public void enableCamera() {
			camera.enabled = true;
			isActiveCamera = true;
		}
		public void disableCamera() {
			camera.enabled = false;
			isActiveCamera = false;
			transform.position = parentObject.transform.position;
		}

		// Camera Movement functions
		private void updateSpeed() {
			if (currentSpeed < topSpeed) {
				currentSpeed += acceleration;
			}
		}
		private void updateYaw(float delta) {
			yaw += delta;
			yaw = MathLib.wrapAngle(yaw);
			transform.localEulerAngles = new Vector3(pitch, yaw, 0);
		}
		private void updatePitch(float delta) {
			pitch += delta;
			pitch = MathLib.wrapAngle(pitch);
			transform.localEulerAngles = new Vector3(pitch, yaw, 0);
		}
		private void moveDown() {
			updateSpeed();
			transform.position += this.currentSpeed * -transform.up * (float)Time.deltaTime;
		}
		private void moveUp() {
			updateSpeed();
			transform.position += this.currentSpeed * transform.up * (float)Time.deltaTime;
		}
		private void moveForward() {
			updateSpeed();
			transform.position += this.currentSpeed * transform.forward * (float)Time.deltaTime;
		}
		private void moveBackward() {
			updateSpeed();
			transform.position += this.currentSpeed * -transform.forward * (float)Time.deltaTime;
		}
		private void strafeLeft() {
			updateSpeed();
			transform.position += this.currentSpeed * -transform.right * (float)Time.deltaTime;
		}
		private void strafeRight() {
			updateSpeed();
			transform.position += this.currentSpeed * transform.right * (float)Time.deltaTime;
		}
		
	}
}
