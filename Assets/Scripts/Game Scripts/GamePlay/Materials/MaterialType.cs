// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI_Game {
	[AddComponentMenu("Environment/Audio/Material Type")]
	public class MaterialType : MonoBehaviour {
		public Materials materialType;
	}
}