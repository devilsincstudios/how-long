/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

/*
 * 
 //TODO Clean this up, it already has functions and stuff in it that have no right to be here.
 * 
 */

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Game {
	[AddComponentMenu("Game State/Controller")]
	public class GameStateController : MonoBehaviour {
		
		private DateTime date;
		private DI_Weather.WeatherTypes weather;
		private GameObject masterMind;
		private CycleTime timeCycle;
		public bool artMode = false;

		// Players
		private GameObject player1Camera;
		private GameObject player2Camera;
		private int numberOfPlayers = 1;

		private Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> p1keyStates = DI_Input.InputManager.getKeyStates(1);
		private Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> p2keyStates = DI_Input.InputManager.getKeyStates(2);
		
		private DI_Game.GameState gameState;
		
		private GameObject devConsole;
		void Start() {
			weather = DI_Weather.WeatherTypes.CLEAR;
			masterMind = GameObject.FindGameObjectWithTag("Master Mind");
			devConsole = GameObject.FindGameObjectWithTag("Console");
			timeCycle = masterMind.GetComponent<CycleTime>();
			int hour = timeCycle.hour;
			date = new DateTime(2000, 1, 1, hour, 0, 0);		
			player1Camera = GameObject.FindGameObjectWithTag("P1 Camera");
			player2Camera = GameObject.FindGameObjectWithTag("P2 Camera");
			player2Camera.SetActive(false);

			// Load saved/default keybindings.
			DI_Input.InputManager.loadKeyBindings();

			// Until we get an actual gui setup for this just hard code the controller type.
			DI_Input.InputManager.setPlayerControllerType(1, DI_Input.ControllerType.PLAYER_ONE_KEYBOARD);
			DI_Input.InputManager.setPlayerControllerType(2, DI_Input.ControllerType.PLAYER_TWO_CONTROLLER_ONE);

			// Register input listener.
			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.addListener("onInput", keyListener);
			setGameState(DI_Game.GameState.PLAYING);
		}

		void Update() {
			if (gameState == DI_Game.GameState.PLAYING) {
				try {
					manageInput(1);
					if (numberOfPlayers == 2) {
						manageInput(2);
					}
				}
				catch (Exception err) {
					UnityEngine.Debug.LogException(err);
				}
			}
			if (gameState == DI_Game.GameState.PAUSED) {
				if (DI_Input.InputManager.getKeyDown(DI_Input.RegisteredKeys.DEV_CONSOLE, 1) == DI_Input.KeyStates.KEY_PRESSED) {
					gameState = DI_Game.GameState.PLAYING;
				}
			}
		}

		// Get the Dictionary of the current keystates so we can save a poll step in each script that needs inputs.
		//=================================================================================================
		public Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> getP1KeyStates() {
			return p1keyStates;
		}

		public Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> getP2KeyStates() {
			return p2keyStates;
		}

		public DI_Input.KeyStates getKeyState(DI_Input.RegisteredKeys key, int player) {
			if (player == 1) {
				return p1keyStates[key];
			}
			else {
				return p2keyStates[key];
			}
		}
		//=================================================================================================

		// Game State handler
		//=================================================================================================
		public DI_Game.GameState getGameState() {
			return gameState;
		}

		public void setGameState(DI_Game.GameState state) {
			gameState = state;
			DI_Events.EventCenter<DI_Game.GameState>.invoke("onGameStateChange", gameState);
		}
		//=================================================================================================
		
		// Listener for key events. This should be the standard listener for key events.
		// This should handle global changes but should not make local changes
		// For example, do not toggle the flashlight from here.
		// It should be part of the charactor class.
		// Its only here as a test and does nothing at the moment.
		// Bringing up the game menu is a good example of what should be in the global listener.
		//=================================================================================================
		public void keyListener(DI_Input.RegisteredKeys key, DI_Input.KeyStates state, int player) {
			if (key == DI_Input.RegisteredKeys.DEV_CONSOLE && player == 1) {
				if (state == DI_Input.KeyStates.KEY_PRESSED) {
					if (gameState == DI_Game.GameState.PAUSED) {
						gameState = DI_Game.GameState.PLAYING;
					}
					else {
						gameState = DI_Game.GameState.PAUSED;
					}
					devConsole.GetComponent<DI_Debug.DebugConsole>().toggle();
				}
			}
			if (key == DI_Input.RegisteredKeys.DEV_MENU && player == 1) {
				Application.LoadLevel("MainMenu");
			}
			if (key == DI_Input.RegisteredKeys.DEV_EXIT && player == 1) {
				Application.Quit();
			}

		}
		//=================================================================================================
		// TODO REFACTOR THIS - Use events.
		// Manage the day/night cycle's time of day.
		//=================================================================================================
		public DateTime getTime() {
			return date;
		}
		
		public void setTime(DateTime newDate) {
			date = newDate;
			timeCycle.forcedUpdate();
		}
		
		public void addHours(float time) {
			while (date.Hour + (int) time >= 24) {
				time = time - 24;
			}
			
			while (date.Hour + (int) time < 0) {
				time = time + 24;
			}
			
			date = new DateTime(2000, 1, 1, date.Hour + (int) time, date.Minute, date.Second);
			timeCycle.forcedUpdate();
		}
		
		public void addMinutes(float time) {
			date.AddMinutes(time);
		}
		//=================================================================================================

		public void setWeather(DI_Weather.WeatherTypes newWeather) {
			weather = newWeather;
		}
		public DI_Weather.WeatherTypes getWeather() {
			return weather;
		}
		
		// Changing the players changes the viewport to be split screen veritical split.
		// P1 on top / P2 on bottom.
		//=================================================================================================
		public void setPlayers(int players) {
			switch (players) {
			case 1:
				player1Camera.camera.fieldOfView = 70;
				player1Camera.camera.rect = new Rect(0f, 0f, 1f, 1f);
				player2Camera.SetActive(false);
				numberOfPlayers = 1;
				break;
			case 2:
				player1Camera.camera.fieldOfView = 30;
				player1Camera.camera.rect = new Rect(0f, 0.5f, 1f, 0.5f);
				player2Camera.camera.fieldOfView = 30;
				player2Camera.camera.rect = new Rect(0f, 0f, 1f, 0.5f);
				player2Camera.SetActive(true);
				numberOfPlayers = 2;
				break;
			default:
				// Ignore this
				break;
			}
		}
		public int getPlayers() {
			return numberOfPlayers;
		}
		//=================================================================================================

		// Send input state changed notification events.
		//=================================================================================================
		private void manageInput(int player) {
			// Store a copy of the currently pressed keys, compare it the last state of the keys.
			// If we have any changes send an event.
			// If we don't add this logic the game takes even more of a performance hit.
			// This part must be as lean as we can make it or we will suffer frame rate hits as its running on fixed update.

			// TODO: Add more logic to this to help prevent some of event calls.
			if (Application.isPlaying) {
				Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> currentKeyStates = DI_Input.InputManager.getKeyStates(1);
				if (player == 1) {
					if (p1keyStates != null) {
						foreach (KeyValuePair<DI_Input.RegisteredKeys, DI_Input.KeyStates> entry in currentKeyStates) {
							if (p1keyStates[entry.Key] != currentKeyStates[entry.Key]) {
								DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.invoke("onInput", entry.Key, entry.Value, 1);
							}
						}
					}
					p1keyStates = currentKeyStates;
				}
				else {
					if (p2keyStates != null) {
						foreach (KeyValuePair<DI_Input.RegisteredKeys, DI_Input.KeyStates> entry in currentKeyStates) {
							if (p2keyStates[entry.Key] != currentKeyStates[entry.Key]) {
								DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.invoke("onInput", entry.Key, entry.Value, 2);
							}
						}
					}
					p2keyStates = currentKeyStates;
				}
			}
		}
		//=================================================================================================
		
		// TODO: Fix the exception thrown about keystate in main thread.
		// I think it has to do with unloading the scene is called before exit and because the scene is unloaded for a moment before it exits it throws the exception.
		public void OnApplicationQuit(){
			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.removeListener("onInput", keyListener);
			DI_Debug.Logger.Instance.cleanUp();
		}
	}
}
