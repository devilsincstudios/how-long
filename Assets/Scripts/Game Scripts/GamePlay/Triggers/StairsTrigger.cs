// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
//  This class is used to let the animator know the character is walking on stairs.
//

using UnityEngine;

namespace DI_Game {
	[AddComponentMenu("Triggers/Stairs/Stairs Trigger")]
	public class StairsTrigger : MonoBehaviour {

		// When something enters the trigger zone.
		public void OnTriggerEnter(Collider triggeringObject)
		{
			Debug.Log("Enter Trigger");
			if (triggeringObject.gameObject.tag == "Player One"
			    || triggeringObject.gameObject.tag == "Player Two")
			{
				triggeringObject.gameObject.GetComponent<DI_Game.CharacterMovement>().enterStairs();
			}
		}

		/*
		// While something is in our trigger zone
		public void OnTriggerStay(Collider triggeringObject)
		{
			Debug.Log("In Trigger");
		}
		*/

		// When something exits our trigger zone
		public void OnTriggerExit(Collider triggeringObject) {
			Debug.Log("Exit Trigger");
			if (triggeringObject.gameObject.tag == "Player One" || triggeringObject.gameObject.tag == "Player Two") {
				triggeringObject.gameObject.GetComponent<DI_Game.CharacterMovement>().exitStairs();
			}
		}
	}
}