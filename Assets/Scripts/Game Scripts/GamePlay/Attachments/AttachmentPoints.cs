// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// AttachmentPoints.cs - Class to keep track of attachments to bones.
//

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

namespace DI_Game {
	[AddComponentMenu("Attachments/Attachment Points")]
	public class AttachmentPoints : MonoBehaviour {
		/**
			Unity doesn't handle dictionaries well...
			Create two lists, one for the keys one for the values and stitch them together at run time.
		*/
		public List<DI_Game.AttachmentTypes> bonesIndex;
		public List<GameObject> bonesValues;
		public Dictionary<AttachmentTypes, GameObject> bones;

		public Dictionary<AttachmentTypes, List<GameObject>> attachmentPoints;

		// This is solely for the editor and is not used else where.
		public GameObject rigNode;

		// This is called multiple times because of a race condition that seems to exist between this and attach to player.
		// If bones is null should prevent it from actually doing anything on the other calls.
		public void loadBones()
		{
			if (bones == null) {
				bones = new Dictionary<AttachmentTypes, GameObject>();
				if (bonesIndex == null) {
					bonesIndex = new List<DI_Game.AttachmentTypes>();
					bonesValues = new List<GameObject>();
				}
				else {
					for (int index = 0; index < bonesIndex.Count; ++index) {
						bones.Add(bonesIndex[index], bonesValues[index]);
					}
				}
			}
		}

		public void Awake() {
			if (attachmentPoints == null) {
				attachmentPoints = new Dictionary<AttachmentTypes, List<GameObject>>();
			}
			loadBones();
			attachmentPoints = new Dictionary<AttachmentTypes, List<GameObject>>();
		}

		public void setBone(AttachmentTypes type, GameObject bone) {
			GameObject boneObject;
			if (!bones.TryGetValue(type, out boneObject)) {
				bones.Add(type, bone);
			}
			else {
				bones[type] = bone;
			}
		}

		public GameObject getBone(AttachmentTypes type) {
			GameObject boneObject;
			loadBones();
			try {
				if (!bones.TryGetValue(type, out boneObject)) {
					throw new Exception("Bone is not defined");
				}
				else {
					return boneObject;
				}
			}
			catch (Exception error) {
				Debug.LogException(error);
				return this.gameObject;
			}
		}

		public void attachToBone(AttachmentTypes type, GameObject attachedObject, Vector3 attachmentOffsetPositon, Vector3 attachmentOffsetRotation) {
			List<GameObject> attachedObjects;
			if (bones == null) {
				loadBones();
			}
			if (attachmentPoints == null) {
				attachmentPoints = new Dictionary<AttachmentTypes, List<GameObject>>();
			}
			if (!attachmentPoints.TryGetValue(type, out attachedObjects)) {
				attachedObjects = new List<GameObject>();
			}
			attachedObjects.Add(attachedObject);
			attachmentPoints.Add(type, attachedObjects);

			// Set the parent.
			attachedObject.transform.parent = bones[type].transform;

			// Set up the rotation offsets.
			float rotationX = bones[type].transform.localRotation.x;
			float rotationY = bones[type].transform.localRotation.y;
			float rotationZ = bones[type].transform.localRotation.z;
			float rotationW = bones[type].transform.localRotation.w;

			// Add the offsets to the bones rotation values.
			rotationX = attachmentOffsetRotation.x;
			rotationY = attachmentOffsetRotation.y;
			rotationZ = attachmentOffsetRotation.z;

			// Set the rotation of the object.
			attachedObject.transform.localRotation = new Quaternion(rotationX, rotationY, rotationZ, rotationW);

			// Set the attachment position as the offset provided.
			attachedObject.transform.localPosition = attachmentOffsetPositon;
		}

		public void detachFromBone(AttachmentTypes type, GameObject attachedObject) {
			List<GameObject> attachedObjects;
			if (bones == null) {
				loadBones();
			}
			if (!attachmentPoints.TryGetValue(type, out attachedObjects)) {
				attachedObjects = new List<GameObject>();
			}
			attachedObjects.Remove(attachedObject);
			attachmentPoints.Add(type, attachedObjects);
			attachedObject.transform.parent = null;
		}

		public void detachAllFromBone(AttachmentTypes type) {
			List<GameObject> attachedObjects = new List<GameObject>();
			attachmentPoints.Add(type, attachedObjects);
		}

		public List<GameObject> getBoneAttachments(AttachmentTypes type) {
			List<GameObject> attachedObjects;
			if (attachmentPoints.TryGetValue(type, out attachedObjects)) {
				return attachedObjects;
			}
			else {
				throw new Exception("No attachment is bound to that bone. " + Enum.GetName(typeof(AttachmentTypes), type));
			}
		}
	}
}