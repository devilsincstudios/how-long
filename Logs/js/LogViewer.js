/*
 *
 * 	Devils Inc Studios
 * 	How Long
 * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
 *	
 *	TODO: Include a description of the file here.
 *
*/

$(document).ready(function() {
  $('#logFileLoader').change(function(event){loadLogFile(event);});
  
  // Add event handlers for toggling log visability.
  $("#All").click( function(){toggleAllOfType('All');});
  $("#AI").click( function(){toggleAllOfType('AI');});
  $("#Client").click( function(){toggleAllOfType('Client');});
  $("#Debug").click( function(){toggleAllOfType('Debug');});
  $("#Error").click( function(){toggleAllOfType('Error');});
  $("#Framework").click( function(){toggleAllOfType('Framework');});
  $("#GameLogic").click( function(){toggleAllOfType('GameLogic');});
  $("#Graphics").click( function(){toggleAllOfType('Graphics');});
  $("#Gui").click( function(){toggleAllOfType('Gui');});
  $("#Input").click( function(){toggleAllOfType('Input');});
  $("#Network").click( function(){toggleAllOfType('Network');});
  $("#Physics").click( function(){toggleAllOfType('Physics');});
  $("#Server").click( function(){toggleAllOfType('Server');});
  $("#System").click( function(){toggleAllOfType('System');});
  $("#Warning").click( function(){toggleAllOfType('Warning');});
});

function loadLogFile(event) {
  var file = document.getElementById("logFileLoader").files[0];
  var reader = new FileReader();
  reader.readAsText(file);
  reader.onload = function(){
    loadFile(reader.result);
  };
  enableAllTypes();
}

function loadFile(jsonData) {
  $("#log-contents").empty();
  $("#Help").hide();
  
  var critical = 0;
  var high = 0;
  var medium = 0;
  var low = 0;
  var info = 0;
  
  try {
    var jsonResults = $.parseJSON(jsonData);
    $.each(jsonResults, function(key, value){
      var logMessage = new Array();
      $.each(jsonResults[key], function(key2, value2){
        $.each(jsonResults[key][key2], function(key3, value3){
          //alert(key3 + "=" + value3);
          if (key3 == "stack trace") {
            var stacktrace = '';
            $.each(jsonResults[key][key2][key3], function(key4, value4){
              if (stacktrace != '') {
                stacktrace = stacktrace + "<br />" + jsonResults[key][key2][key3][key4][key4];
              }
              else {
                stacktrace = jsonResults[key][key2][key3][key4][key4];
              }
            });
            logMessage[key3] = stacktrace;
          }
          else {
            logMessage[key3] = value3;
          }
        });
      });
      //alert(logMessage);
      logMessage['log level'] = ucfirst(logMessage['log level']);
      logMessage['component'] = ucfirst(logMessage['component']);
      logMessage['component'] = logMessage['component'].replace(" ", "");
      //function addLogEntry(priority, type, title, message, trace, time, screenshot) {
      addLogEntry(logMessage['log level'], logMessage['component'], logMessage['title'], logMessage['message'], logMessage['stack trace'], logMessage['time'], logMessage['screenshot']);
      var logLevel = logMessage['log level'];
      if (logLevel == 'Critical') { ++critical; }
      if (logLevel == 'High') { ++high; }
      if (logLevel == 'Medium') { ++medium; }
      if (logLevel == 'Low') { ++low; }
      if (logLevel == 'Info') { ++info; }
    });
    updateNumbers(critical, high, medium, low, info);
    if ($('#HowLong').hasClass('logLoaded')) {
      $('#HowLong').toggleClass('logLoaded'); 
    }
  }
  catch (error) {
    $('#Help').html("<div class='alert alert-danger'><h3 style='text-align: center;'>Please select a different log file.<br />The one selected, could not be loaded.</h3></div>");
    $('#Help').show();
    updateNumbers(0,0,0,0,0);
    if (!$('#HowLong').hasClass('logLoaded')) {
      $('#HowLong').toggleClass('logLoaded'); 
    }
  }
}

function ucfirst(string) {
  return string.charAt(0).toUpperCase() + string.substr(1);
}

function updateNumbers(critical, high, medium, low, info) {
  document.getElementById('Critical Count').innerHTML = critical;
  document.getElementById('High Count').innerHTML = high;
  document.getElementById('Medium Count').innerHTML = medium;
  document.getElementById('Low Count').innerHTML = low;
  document.getElementById('Info Count').innerHTML = info;
}

function addLogEntry(priority, type, title, message, trace, time, screenshot) {
  type = type.replace(" ", "");
  var logEntry = document.createElement("div");
    $(logEntry).addClass("panel");
    $(logEntry).addClass("panel-default");
  var logHeader = document.createElement("div");
    $(logHeader).addClass("panel-heading");
    $(logHeader).addClass(priority);
  var priorityLabel = document.createElement("span");
    $(priorityLabel).addClass("label");
    $(priorityLabel).addClass("label-" + priority);
    priorityLabel.innerHTML = priority;
  var typeLabel = document.createElement("span");
    $(typeLabel).addClass("label");
    $(typeLabel).addClass("label-" + type);
    $(typeLabel).attr("style", "margin-right: 10px;");
    typeLabel.innerHTML = type;
  var titleNode = document.createTextNode(time + " " + title);
  var titleExpander = document.createElement("span");
    $(titleExpander).addClass("glyphicon");
    $(titleExpander).addClass("glyphicon-chevron-down");
    $(titleExpander).attr("style", "float:right");
    $(titleExpander).click(function(){
      var parent = $(titleExpander).parent().parent();
      parent.children('.well').toggle();
      $(titleExpander).toggleClass('glyphicon-chevron-up');
      $(titleExpander).toggleClass('glyphicon-chevron-down');
    });
  // Create the log header
  logHeader.appendChild(priorityLabel);
  logHeader.appendChild(typeLabel);
  logHeader.appendChild(titleNode);
  logHeader.appendChild(titleExpander);
  logEntry.appendChild(logHeader);
  
  var panelBody = document.createElement("div");
    $(panelBody).addClass("panel-body");
  var panelMessage = document.createElement("div");
    $(panelMessage).addClass("well");
    $(panelMessage).addClass("well-sm");
    panelMessage.innerHTML = message;
    $(panelMessage).attr('style', 'margin-top: 10px;');
  var panelTrace = document.createElement("div");
    $(panelTrace).addClass("well");
    $(panelTrace).addClass("well-sm");
  var stackTrace = document.createElement("code");
    stackTrace.innerHTML = trace;
  if (screenshot != '') {
    var panelSS = document.createElement("div");
      $(panelSS).addClass("well");
      $(panelSS).addClass("well-sm");
      $(panelSS).attr("style", "text-align: center");
    var panelImage = document.createElement("img");
      $(panelImage).attr("src", screenshot);
      $(panelImage).attr("alt", screenshot);
    panelSS.appendChild(panelImage);
  }
  
  // Create the log body
  panelTrace.appendChild(stackTrace);
  logEntry.appendChild(panelMessage);
  logEntry.appendChild(panelTrace); 
  
  if (screenshot != '') {
    logEntry.appendChild(panelSS);
  }
  
  // Attach it to the main content div and set it to collapsed
  $("#log-contents").append(logEntry);
  $("#log-contents").find('.well').hide();
}

function toggleAllOfType(type) {
  type = type.replace(" ", "");
  if (!$('#HowLong').hasClass('logLoaded')) {
    if (type == "All") {
      if ($("#" + type).hasClass("TypeDisabled")) {
        enableAllOfType("AI");
        enableAllOfType("Client");
        enableAllOfType("Debug");
        enableAllOfType("Error");
        enableAllOfType("Framework");
        enableAllOfType("Game Logic");
        enableAllOfType("Graphics");
        enableAllOfType("Gui");
        enableAllOfType("Input");
        enableAllOfType("Network");
        enableAllOfType("Physics");
        enableAllOfType("Server");
        enableAllOfType("System");
        enableAllOfType("Warning");
      }
      else {
        disableAllOfType("AI");
        disableAllOfType("Client");
        disableAllOfType("Debug");
        disableAllOfType("Error");
        disableAllOfType("Framework");
        disableAllOfType("Game Logic");
        disableAllOfType("Graphics");
        disableAllOfType("Gui");
        disableAllOfType("Input");
        disableAllOfType("Network");
        disableAllOfType("Physics");
        disableAllOfType("Server");
        disableAllOfType("System");
        disableAllOfType("Warning");
      }
      $("#" + type).toggleClass(type);
      $("#" + type).toggleClass("TypeDisabled");
    }
    else {
      var typeElement = $("#log-contents").find(".label-" + type);
      typeElement.parent().parent().toggle();
      $("#" + type).toggleClass(type);
      $("#" + type).toggleClass("TypeDisabled");
    }
  }
}

function enableAllOfType(type) {
  type = type.replace(" ", "");
  if (!$('#HowLong').hasClass('logLoaded')) {
    if ($("#" + type).hasClass('TypeDisabled')) {
      $("#" + type).toggleClass(type);
      $("#" + type).toggleClass("TypeDisabled");
      try {
        var typeElement = $("#log-contents").find(".label-" + type);
        typeElement.parent().parent().show();
      }
      catch (error) {};
    }
  }
}

function disableAllOfType(type) {
  type = type.replace(" ", "");
  if (!$('#HowLong').hasClass('logLoaded')) {
    if (!$("#" + type).hasClass('TypeDisabled')) {
      $("#" + type).toggleClass(type);
      $("#" + type).toggleClass("TypeDisabled");
      try {
        var typeElement = $("#log-contents").find(".label-" + type);
        typeElement.parent().parent().hide();
      }
      catch (error) {};
    }
  }
}

function enableAllTypes() {
  enableAllOfType("AI");
  enableAllOfType("Client");
  enableAllOfType("Debug");
  enableAllOfType("Error");
  enableAllOfType("Framework");
  enableAllOfType("Game Logic");
  enableAllOfType("Graphics");
  enableAllOfType("Gui");
  enableAllOfType("Input");
  enableAllOfType("Network");
  enableAllOfType("Physics");
  enableAllOfType("Server");
  enableAllOfType("System");
  enableAllOfType("Warning");
}

